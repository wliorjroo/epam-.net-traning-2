﻿using System.Collections.Generic;
using System.Text;

namespace TextComposition
{
    abstract class TextComponent : ITextComponent
    {
        public abstract IList<ITextComponent> Components { get; }

        public abstract ComponentType ComponentType { get; }

        public override int GetHashCode()
        {
            int result = 0;
            bool isFirst = true;
            foreach (var item in Components)
            {
                if (isFirst)
                {
                    isFirst = false;
                    result = item.GetHashCode();
                }
                else
                {
                    // TODO hash function
                    result ^= item.GetHashCode();
                }

            }
            return result;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder(Components.Count);
            foreach (var item in Components)
            {
                stringBuilder.Append(item.ToString());
            }
            return stringBuilder.ToString();
        }
    }
}
