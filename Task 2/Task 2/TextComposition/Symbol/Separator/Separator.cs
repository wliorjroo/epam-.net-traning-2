﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.SentenceComponent;
using TextComposition.Symbol.Literal;

namespace TextComposition.Symbol.Separator
{
    class Separator : Symbol, ISeparator
    {
        public IList<ILiteral> SeparatorLiterals { get; }

        public Separator(ILiteral[] literals, SeparatorType separatorType)
        {
            if (literals == null)
                throw new ArgumentNullException(nameof(literals));
            SeparatorLiterals = new List<ILiteral>(literals);
            SeparatorType = separatorType;
        }

        public sealed override SymbolType SymbolType
        {
            get {
                return SymbolType.Separator;
            }
        }

        public SeparatorType SeparatorType { get; }

        public override IList<ITextComponent> Components
        {
            get {
                return SeparatorLiterals.ToList<ITextComponent>();
            }
        }

        public SentenceComponentType SentenceComponentType
        {
            get {
                return SentenceComponentType.Separator;
            }
        }
    }
}
