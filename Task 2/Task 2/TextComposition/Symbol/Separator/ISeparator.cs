﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.SentenceComponent;
using TextComposition.Symbol.Literal;

namespace TextComposition.Symbol.Separator
{
    public interface ISeparator : ISymbol, ISentenceComponent
    {
        SeparatorType SeparatorType { get; }
        IList<ILiteral> SeparatorLiterals { get; }
    }
}
