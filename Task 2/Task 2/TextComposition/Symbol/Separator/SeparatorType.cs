﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TextComposition.Symbol.Separator
{
    public enum SeparatorType
    {
        Word,
        Sentence,
        Paragraph
    }
}
