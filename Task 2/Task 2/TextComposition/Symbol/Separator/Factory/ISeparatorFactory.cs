﻿namespace TextComposition.Symbol.Separator.Factory
{
    interface ISeparatorFactory
    {
        ISeparator Create(string separatorLiterals, SeparatorType separatorType);
    }
}
