﻿using System;
using System.Collections.Generic;
using TextComposition.Symbol.Literal;
using TextComposition.Symbol.Literal.Factory;

namespace TextComposition.Symbol.Separator.Factory
{
    class SeparatorFactory : ISeparatorFactory
    {
        private readonly ILiteralFactory _literalFactory;
        private readonly Dictionary<string, ISeparator> _separators;

        public SeparatorFactory(ILiteralFactory literalFactory)
        {
            _literalFactory = literalFactory;
            _separators = new Dictionary<string, ISeparator>();
        }

        public ISeparator Create(string separatorLiterals, SeparatorType separatorType)
        {
            if (separatorLiterals == null)
                throw new ArgumentNullException(nameof(separatorLiterals));
            if (separatorLiterals.Length < 1)
                throw new ArgumentOutOfRangeException(nameof(separatorLiterals));

            if (!_separators.ContainsKey(separatorLiterals))
            {
                var result = new Separator(GetLiterals(separatorLiterals), separatorType);
                _separators.Add(separatorLiterals, result);
                return result;
            }
            else
            {
                return _separators[separatorLiterals];
            }
        }

        private ILiteral[] GetLiterals(string literals)
        {
            var result = new ILiteral[literals.Length];
            for (int i = 0; i < literals.Length; i++)
            {
                result[i]= _literalFactory.Create(literals[i].ToString(), LiteralType.SpecialSymbol);
            }
            return result;
        }
    }
}
