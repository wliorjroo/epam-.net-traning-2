﻿using System.Collections.Generic;

namespace TextComposition.Symbol.Literal
{
    class Literal : Symbol, ILiteral
    {
        public char Character { get; }

        public Literal(char literal)
        {
            Character = literal;
            Components = null;
        }

        public Literal(char literal, LiteralType literalType) : this(literal)
        {
            LiteralType = literalType;
        }

        public sealed override SymbolType SymbolType
        {
            get {
                return SymbolType.Literal;
            }
        }

        public LiteralType LiteralType { get; }

        public sealed override IList<ITextComponent> Components { get; }

        public sealed override int GetHashCode()
        {
            return Character.GetHashCode();
        }

        public sealed override string ToString()
        {
            return Character.ToString();
        }

        public override bool Equals(object obj)
        {
            var literal = obj as Literal;
            return literal != null &&
                   Character == literal.Character;
        }
    }
}
