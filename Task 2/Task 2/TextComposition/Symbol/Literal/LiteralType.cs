﻿namespace TextComposition.Symbol.Literal
{
    public enum LiteralType
    {
        Alphabet,
        Numeric,
        SpecialSymbol
    }
}
