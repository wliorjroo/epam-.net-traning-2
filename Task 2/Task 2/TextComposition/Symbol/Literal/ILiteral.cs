﻿namespace TextComposition.Symbol.Literal
{
    public interface ILiteral : ISymbol
    {
        LiteralType LiteralType { get; }
        char Character { get; }
    }
}
