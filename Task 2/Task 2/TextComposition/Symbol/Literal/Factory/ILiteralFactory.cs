﻿namespace TextComposition.Symbol.Literal.Factory
{
    interface ILiteralFactory
    {
        ILiteral Create(string literal, LiteralType literalType);
    }
}
