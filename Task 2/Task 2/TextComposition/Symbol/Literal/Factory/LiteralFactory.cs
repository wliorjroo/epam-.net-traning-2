﻿using System;
using System.Collections.Generic;
using TextComposition.Factory;

namespace TextComposition.Symbol.Literal.Factory
{
    class LiteralFactory : ILiteralFactory
    {
        private readonly Dictionary<string, ILiteral> _literals;

        public LiteralFactory()
        {
            _literals = new Dictionary<string, ILiteral>();
        }

        public ILiteral Create(string literal, LiteralType literalType)
        {
            if (literal == null)
                throw new ArgumentNullException(nameof(literal));
            if (literal.Length != 1)
                throw new ArgumentOutOfRangeException(nameof(literal));

            if (!_literals.ContainsKey(literal))
            {
                var result = new Literal(literal[0], literalType);
                _literals.Add(literal, result);
                return result;
            }
            else
            {
                return _literals[literal];
            }
        }
    }
}
