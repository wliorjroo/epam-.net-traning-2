﻿using System.Collections.Generic;

namespace TextComposition.Symbol
{
    abstract class Symbol : TextComponent, ISymbol
    {
        public abstract SymbolType SymbolType { get; }

        public override sealed ComponentType ComponentType
        {
            get {
                return ComponentType.Symbol;
            }
        }
    }
}
