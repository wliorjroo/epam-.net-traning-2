﻿namespace TextComposition.Symbol
{
    public enum SymbolType
    {
        Literal,
        Separator
    }
}
