﻿namespace TextComposition.Symbol
{
    public interface ISymbol : ITextComponent
    {
        SymbolType SymbolType { get; }
    }
}
