﻿namespace TextComposition
{
    public enum ComponentType
    {
        Symbol,
        Word,
        Sentence,
        TextPart,
        Text
    }
}
