﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.Sentence;

namespace TextComposition.Text
{
    public interface IText : ITextComponent
    {
        IList<ISentence> Sentences { get; }
    }
}
