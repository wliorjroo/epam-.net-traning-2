﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.Sentence;

namespace TextComposition.Text.Factory
{
    interface ITextFactory
    {
        IText Create();
        IText Create(IList<ISentence> sentences);
    }
}
