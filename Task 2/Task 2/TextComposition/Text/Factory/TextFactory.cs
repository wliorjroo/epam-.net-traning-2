﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.Sentence;

namespace TextComposition.Text.Factory
{
    class TextFactory : ITextFactory
    {
        public IText Create()
        {
            return new Text();
        }

        public IText Create(IList<ISentence> sentences)
        {
            return new Text(sentences);
        }
    }
}
