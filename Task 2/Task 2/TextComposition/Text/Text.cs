﻿using System;
using System.Collections.Generic;
using System.Linq;
using TextComposition.Sentence;

namespace TextComposition.Text
{
    class Text : TextComponent, IText
    {
        public Text() : this(new List<ISentence>())
        {
        }

        public Text(IList<ISentence> sentences)
        {
            Sentences = sentences ?? throw new ArgumentNullException(nameof(sentences));
        }

        public sealed override IList<ITextComponent> Components
        {
            get {
                return Sentences.ToList<ITextComponent>();
            }
        }

        public sealed override ComponentType ComponentType
        {
            get {
                return ComponentType.Text;
            }
        }

        public IList<ISentence> Sentences { get; }
    }
}
