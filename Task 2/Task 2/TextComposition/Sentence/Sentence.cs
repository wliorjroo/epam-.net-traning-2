﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.SentenceComponent;
using TextComposition.Symbol.Separator;
using TextComposition.Word;

namespace TextComposition.Sentence
{
    class Sentence : TextComponent, ISentence
    {
        public IList<IWord> GetWords()
        {
            var result = new List<IWord>(SentenceComponents.OfType<IWord>());
            if (result.Count > 0)
            {
                return result;
            }
            return null;
        }

        public IList<ISeparator> GetWordSeparators()
        {
            var result = new List<ISeparator>(SentenceComponents.OfType<ISeparator>());
            if (result.Count > 0)
            {
                result.RemoveAt(result.Count - 1);
                return result;
            }
            return null;
        }

        public ISeparator SentenceSeparator
        {
            get {
                return SentenceComponents.Last() as ISeparator;
            }
        }

        public Sentence()
        {
            SentenceComponents = new List<ISentenceComponent>();
        }

        public Sentence(IList<ISentenceComponent> sentenceComponents)
        {
            SentenceComponents = sentenceComponents;
        }

        #region old
        //public Sentence(IList<IWord> words, IList<ISeparator> wordSeparators, ISeparator sentenceSeparator)
        //{
        //    if (words == null)
        //        throw new ArgumentNullException(nameof(words));
        //    if (wordSeparators == null)
        //        throw new ArgumentNullException(nameof(wordSeparators));
        //    if (sentenceSeparator == null)
        //        throw new ArgumentNullException(nameof(sentenceSeparator));
        //    if (words.Count != wordSeparators.Count - 1)
        //        throw new ArgumentException("Inconsistency of the number of \"words\" to the number of \"word separators\"");
        //    Words = words;
        //    WordSeparators = wordSeparators;
        //    SentenceSeparator = sentenceSeparator;
        //}

        //public sealed override IList<ITextComponent> Components
        //{
        //    get {
        //        var result = new List<ITextComponent>();
        //        for (int i = 0; i < Words.Count - 1; i++)
        //        {
        //            result.Add(Words[i]);
        //            result.Add(WordSeparators[i]);
        //        }
        //        result.Add(Words[Words.Count - 1]);
        //        result.Add(SentenceSeparator);
        //        return result;
        //    }
        //}
        #endregion
        public sealed override ComponentType ComponentType
        {
            get {
                return ComponentType.Sentence;
            }
        }

        public IList<ISentenceComponent> SentenceComponents { get; }

        public sealed override IList<ITextComponent> Components
        {
            get {
                return SentenceComponents.ToList<ITextComponent>();
            }
        }
    }
}
