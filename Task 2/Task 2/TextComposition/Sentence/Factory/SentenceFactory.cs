﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.SentenceComponent;

namespace TextComposition.Sentence.Factory
{
    class SentenceFactory : ISentenceFactory
    {
        public ISentence Create()
        {
            return new Sentence();
        }

        public ISentence Create(IList<ISentenceComponent> components)
        {
            return new Sentence(components);
        }
    }
}
