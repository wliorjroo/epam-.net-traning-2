﻿using System.Collections.Generic;
using TextComposition.SentenceComponent;

namespace TextComposition.Sentence.Factory
{
    interface ISentenceFactory
    {
        ISentence Create();

        ISentence Create(IList<ISentenceComponent> components);
    }
}