﻿namespace TextComposition.Sentence
{
    public enum SentenceType
    {
        Question,
        Exclamatory,
        Simple
    }
}