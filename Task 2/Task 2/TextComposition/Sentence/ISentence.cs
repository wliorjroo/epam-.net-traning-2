﻿using System.Collections.Generic;
using TextComposition.SentenceComponent;
using TextComposition.Symbol.Separator;
using TextComposition.Word;

namespace TextComposition.Sentence
{
    public interface ISentence : ITextComponent
    {
        IList<ISentenceComponent> SentenceComponents { get; }
        IList<IWord> GetWords();
        IList<ISeparator> GetWordSeparators();
        ISeparator SentenceSeparator { get; }
    }
}
