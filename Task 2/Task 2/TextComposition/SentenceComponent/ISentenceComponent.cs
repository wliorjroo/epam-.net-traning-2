﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TextComposition.SentenceComponent
{
    public interface ISentenceComponent : ITextComponent
    {
        SentenceComponentType SentenceComponentType { get; }
    }
}
