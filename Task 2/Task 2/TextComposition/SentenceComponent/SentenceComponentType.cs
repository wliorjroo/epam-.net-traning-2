﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TextComposition.SentenceComponent
{
    public enum SentenceComponentType
    {
        Word,
        Separator
    }
}
