﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.Sentence;
using TextComposition.SentenceComponent;
using TextComposition.Symbol.Literal;
using TextComposition.Symbol.Separator;
using TextComposition.Text;
using TextComposition.Word;

namespace TextComposition.Factory
{
    public interface ITextComponentFactory
    {
        ILiteral CreateLiteral(string literal);

        ISeparator CreateSentenceSeparator(string separator);

        ISeparator CreateWordSeparator(string separator);

        IWord CreateWord(string word);

        ISentence CreateSentence();

        ISentence CreateSentence(IList<ISentenceComponent> sentenceComponents);

        IText CreateText();

        IText CreateText(IList<ISentence> sentences);
    }
}
