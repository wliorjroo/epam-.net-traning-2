﻿namespace TextComposition.Factory
{
    public static class TextFactory
    {
        public static ITextComponentFactory CreateTextFactory()
        {
            return new TextComponentFactory();
        }
    }
}
