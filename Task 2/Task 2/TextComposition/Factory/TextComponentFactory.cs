﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.Sentence;
using TextComposition.Sentence.Factory;
using TextComposition.SentenceComponent;
using TextComposition.Symbol.Literal;
using TextComposition.Symbol.Literal.Factory;
using TextComposition.Symbol.Separator;
using TextComposition.Symbol.Separator.Factory;
using TextComposition.Text;
using TextComposition.Text.Factory;
using TextComposition.Word;
using TextComposition.Word.Factory;

namespace TextComposition.Factory
{
    class TextComponentFactory : ITextComponentFactory
    {
        private ILiteralFactory _literalFactory;
        private ISeparatorFactory _separatorFactory;
        private IWordFactory _wordFactory;
        private ISentenceFactory _sentenceFactory;
        private ITextFactory _textFactory;

        public TextComponentFactory()
        {
            _literalFactory = new LiteralFactory();
            _separatorFactory = new SeparatorFactory(_literalFactory);
            _wordFactory = new WordFactory(_literalFactory);
            _sentenceFactory = new SentenceFactory();
            _textFactory = new Text.Factory.TextFactory();
        }

        public ILiteral CreateLiteral(string literal)
        {
            return _literalFactory.Create(literal, LiteralType.Alphabet);
        }

        public ISentence CreateSentence()
        {
            return _sentenceFactory.Create();
        }

        public ISentence CreateSentence(IList<ISentenceComponent> sentenceComponents)
        {
            return _sentenceFactory.Create(sentenceComponents);
        }

        public ISeparator CreateSentenceSeparator(string separator)
        {
            return _separatorFactory.Create(separator, SeparatorType.Sentence);
        }

        public IText CreateText()
        {
            return _textFactory.Create();
        }

        public IText CreateText(IList<ISentence> sentences)
        {
            return _textFactory.Create(sentences);
        }

        public IWord CreateWord(string word)
        {
            return _wordFactory.Create(word);
        }

        public ISeparator CreateWordSeparator(string separator)
        {
            return _separatorFactory.Create(separator, SeparatorType.Word);
        }
    }
}
