﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.Symbol.Literal;
using TextComposition.Symbol.Literal.Factory;

namespace TextComposition.Word.Factory
{
    class WordFactory : IWordFactory
    {
        private readonly ILiteralFactory _literalFactory;
        private readonly Dictionary<string, IWord> _words;

        public WordFactory(ILiteralFactory literalFactory)
        {
            _literalFactory = literalFactory;
            _words = new Dictionary<string, IWord>();
        }

        public IWord Create(string word)
        {
            if (word == null)
                throw new ArgumentNullException(nameof(word));
            if (word.Length == 0)
                throw new ArgumentOutOfRangeException(nameof(word));

            if (!_words.ContainsKey(word))
            {
                var literals = new ILiteral[word.Length];
                int i = 0;
                foreach (char item in word)
                {
                    literals[i] = _literalFactory.Create(item.ToString(), LiteralType.Alphabet);
                    i++;
                }
                return new Word(literals);
            }
            else
            {
                return _words[word];
            }
        }

        public IWord Create(ILiteral[] literals)
        {
            return new Word(literals);
        }
    }
}
