﻿using TextComposition.Symbol.Literal;

namespace TextComposition.Word.Factory
{
    interface IWordFactory
    {
        IWord Create(ILiteral[] literals);

        IWord Create(string word);
    }
}
