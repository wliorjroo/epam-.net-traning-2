﻿using System.Collections.Generic;
using TextComposition.SentenceComponent;
using TextComposition.Symbol.Literal;

namespace TextComposition.Word
{
    public interface IWord : ITextComponent, ISentenceComponent
    {
        IList<ILiteral> Literals { get; }
    }
}
