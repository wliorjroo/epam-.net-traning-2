﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.SentenceComponent;
using TextComposition.Symbol.Literal;

namespace TextComposition.Word
{
    class Word : TextComponent, IWord
    {
        public IList<ILiteral> Literals { get; }

        public Word() 
        {
            Literals = new List<ILiteral>();
        }

        public Word(ILiteral[] characters)
        {
            Literals = new List<ILiteral>(characters);
        }

        public override IList<ITextComponent> Components
        {
            get {
                return Literals.ToList<ITextComponent>();
            }
        }

        public override ComponentType ComponentType
        {
            get {
                return ComponentType.Word;
            }
        }

        public SentenceComponentType SentenceComponentType
        {
            get {
                return SentenceComponentType.Word;
            }
        }

        public override bool Equals(object obj)
        {
            var word = obj as Word;
            if (word == null)
            {
                return false;
            }
            if (Literals.Count != word.Literals.Count)
            {
                return false;
            }
            for (int i = 0; i < Literals.Count; i++)
            {
                Literals[i].Equals(word.Literals[i]);
            }
            return true;
        }
    }
}
