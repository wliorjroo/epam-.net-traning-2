﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TextComposition
{
    public interface ITextComponent
    {
        string ToString();
        IList<ITextComponent> Components { get; }
        ComponentType ComponentType { get; }
    }
}
