﻿using Exercise_1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition;
using TextComposition.Text;
using TextParser;
using TextParser.Exceptions;

namespace Exercise_1
{
    static class Program
    {
        const string BadArgumentsMessage = "Bad Parameters. Param line: {Filepath} {Number} {Number}";
        const string ParserExceptionMessage = "Text was parsed with errors. Text that has been parsed :";
        const string UndefinedExceptionMessage = "An undefined error has occurred: ";
        const string OutputDelimiter = "--------------------";

        static void Main(string[] args)
        {
            int wordsLength, LeadConsonantLetterWordLength;
            if (args.Length != 3
                || !int.TryParse(args[1], out wordsLength)
                || !int.TryParse(args[2], out LeadConsonantLetterWordLength))
            {
                Console.WriteLine(BadArgumentsMessage);
            }
            else
            {
                IText text;
                try
                {
                    text = Parser.TextParser.Parse(args[0]);
                }
                catch (ParseException exception)
                {
                    Console.WriteLine(ParserExceptionMessage);
                    text = exception.ParsedText;
                }
                catch (Exception exception)
                {
                    Console.WriteLine(UndefinedExceptionMessage);
                    //Console.WriteLine(exception.Message);
                    return;
                }

                WriteToConsole(text);
                List<BaseItem> items = new List<BaseItem>();
                items.Add(new SortSentencesByWordsCount());
                items.Add(new SameWordsInQuestionSentences(wordsLength));
                items.ExecuteItem(text, WriteToConsole);
            }

            Console.ReadKey();
        }

        public static void ExecuteItem(this IEnumerable<BaseItem> items, 
            IText component, 
            Action<IEnumerable<ITextComponent>> action)
        {
            foreach (var item in items)
            {
                action(item.Execute(component));
            }
        }

        public static void WriteToConsole(IEnumerable<ITextComponent> components)
        {
            Console.WriteLine(OutputDelimiter);
            foreach (var item in components)
            {
                Console.WriteLine(item.ToString());
                Console.WriteLine(OutputDelimiter);
            }
        }

        public static void WriteToConsole(ITextComponent component)
        {
            if (component != null)
            {
                Console.WriteLine(OutputDelimiter);
                foreach (var item in component.Components)
                {
                    Console.WriteLine(item.ToString());
                }
                Console.WriteLine(OutputDelimiter);
            }
        }
    }
}
