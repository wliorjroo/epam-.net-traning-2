﻿using System.Collections.Generic;
using TextComposition;
using TextComposition.Text;

namespace Exercise_1.Items
{
    interface IItem
    {
        IEnumerable<ITextComponent> Execute(IText text);
    }
}