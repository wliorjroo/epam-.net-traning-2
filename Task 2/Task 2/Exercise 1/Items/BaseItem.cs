﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition;
using TextComposition.Text;

namespace Exercise_1.Items
{
    abstract class BaseItem : IItem
    {
        public abstract IEnumerable<ITextComponent> Execute(IText text);
    }
}
