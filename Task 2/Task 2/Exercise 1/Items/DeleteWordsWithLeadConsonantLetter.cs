﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition;
using TextComposition.Sentence;
using TextComposition.Text;
using TextComposition.Word;

namespace Exercise_1.Items
{
    class DeleteWordsWithLeadConsonantLetter : BaseItem
    {
        private int _wordLength;
        private char[] vowelLetters = new []
        {
            'a',
            'e',
            'i',
            'o',
            'u',
            'y'
        };

        public DeleteWordsWithLeadConsonantLetter(int wordLength)
        {
            _wordLength = wordLength;
        }

        public override IEnumerable<ITextComponent> Execute(IText text)
        {
            LinkedList<ISentenceComponents> components = new LinkedList<ISentenceComponents>();

            //components.

            foreach (var sentence in text.Sentences)
            {
                //var newSentenceComponents = sentence.SentenceComponents.where

                var wordsWithLeadConsonantLetter = sentence.GetWords().Where(x => IsLetterConsonant(x.Literals.First().Character));
                foreach (var item in wordsWithLeadConsonantLetter)
                {
                    int index = sentence.SentenceComponents.IndexOf(item);
                    sentence.SentenceComponents.RemoveAt(index);
                }
            }

            //var result = text.Sentences.Select(x => x.SentenceComponents.IndexOf(x.))
                //.Where(x => IsLetterConsonant(x.Literals.First().Character));

            throw new NotImplementedException();
        }

        public bool IsLetterConsonant(char letter)
        {
            if (!char.IsLetter(letter))
            {
                return false;
            }
            foreach (var item in vowelLetters)
            {
                if (char.ToLower(item) == letter)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
