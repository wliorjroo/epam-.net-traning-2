﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TextComposition;
using TextComposition.Text;
using TextComposition.Word;
using TextParser.Configuration;

namespace Exercise_1.Items
{
    class SameWordsInQuestionSentences : BaseItem
    {
        private int _wordLenth;

        public SameWordsInQuestionSentences(int wordLenth)
        {
            _wordLenth = wordLenth;
        }

        public override IEnumerable<ITextComponent> Execute(IText text)
        {
            var result = text.Sentences.Where(x => x.SentenceSeparator.ToString() == "?")
                .SelectMany(x => x.GetWords())
                .Where(x => x.Literals.Count == _wordLenth)
                .Distinct();

            return result; 
        }
    }
}
