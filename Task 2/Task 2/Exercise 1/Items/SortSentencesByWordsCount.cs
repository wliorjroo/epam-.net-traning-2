﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition;
using TextComposition.Factory;
using TextComposition.Sentence;
using TextComposition.Text;
using TextComposition.Word;

namespace Exercise_1.Items
{
    class SortSentencesByWordsCount : BaseItem
    {
        public SortSentencesByWordsCount()
        {
        }

        public override IEnumerable<ITextComponent> Execute(IText text)
        {
            var result = text.Sentences.OrderBy(x => x.SentenceComponents.OfType<IWord>().Count());

            return result;
        }
    }
}
