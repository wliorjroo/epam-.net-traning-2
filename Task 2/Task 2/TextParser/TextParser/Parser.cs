﻿using System;
using System.Collections.Generic;
using System.IO;
//using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TextComposition;
using TextComposition.Factory;
using TextComposition.SentenceComponent;
using TextComposition.Text;
using TextParser.Exceptions;

namespace TextParser.TextParser
{
    class Parser : IParser
    {
        private readonly ITextComponentFactory _factory;

        // TODO read/build pattern from config file
        private const string _pattern = @"(\b(((\w|\w*\-\w*|\w*\'\w*)+)(\s*|\,\s*|\s+\-\s+|\;\s+))+)(\.\s*|\?|\!|\?\!|\.\.\.)";
        // charGroups
        // TODO return groups numbers after build pattern
        private const int _sentenceGroup = 1;
        private const int _wordWithWordSeparatorsGroup = 2;
        private const int _wordsGroup = 3;
        private const int _literalsGroup = 4;
        private const int _wordSeparartorsGroup = 5;
        private const int _sentenceSeparatorGroup = 6;

        public Parser()
        {
            _factory = TextFactory.CreateTextFactory();
        }

        public IText Parse(string filePath)
        {
            IText result = _factory.CreateText();

            using (FileStream fileStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (BufferedStream bufferedStream = new BufferedStream(fileStream))
            using (StreamReader streamReader = new StreamReader(bufferedStream))
            {
                if (!streamReader.EndOfStream)
                {
                    while (!streamReader.EndOfStream)
                    {
                        ParseLine(streamReader.ReadLine(), result);
                    }
                }
            }

            return result;
        }

        private void ParseLine(string part, IText text)
        {
            try
            {
                foreach (Match match in Regex.Matches(part, _pattern, RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled))
                {
                    int capacity = match.Groups[_wordsGroup].Length * 2;
                    List<ISentenceComponent> components = new List<ISentenceComponent>(capacity);

                    int i = 0;

                    foreach (Capture capture in match.Groups[_wordsGroup].Captures)
                    {
                        var word = _factory.CreateWord(capture.Value);
                        components.Add(word);
                        if (i + 1 != match.Groups[_wordsGroup].Captures.Count)
                        {
                            var separator = _factory.CreateWordSeparator(match.Groups[_wordSeparartorsGroup].Captures[i].Value);
                            components.Add(separator);
                        }

                        i++;
                    }

                    var sentenceSeparator = _factory.CreateSentenceSeparator(match.Groups[_sentenceSeparatorGroup].Captures[0].Value);
                    components.Add(sentenceSeparator);

                    text.Sentences.Add(_factory.CreateSentence(components));
                }
            }
            catch (Exception exception)
            {
                throw new ParseException(exception.Message, text);
            }
        }
    }
}
