﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TextParser.Configuration
{
    public static class Settings
    {
        public static string[] WordSeparators { get; private set; }

        public static string[] SentenceSeparators { get; private set; }

        public static string[] Literals { get; private set; }

        static Settings()
        {
            WordSeparators = DefaultSettings.WordSeparatorPatterns;
            SentenceSeparators = DefaultSettings.SentenceSeparatorPatterns;
            Literals = DefaultSettings.LiteralPatterns;
        }
    }
}
