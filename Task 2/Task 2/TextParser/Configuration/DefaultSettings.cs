﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TextParser.Configuration
{
    static class DefaultSettings
    {
        // default regEx params

        public static readonly string[] WordSeparatorPatterns = new []
        {
            @"\s*",
            @"\,\s+",
            @"\s+\-\s+",
            @"\;\s+",
            @"\:\s+",
        };

        public static readonly string[] WordSeparators = new[]
        {
            " ",
            ", ",
            " - ",
            "; ",
            ": ",
        };

        public static readonly string[] SentenceSeparatorPatterns = new []
        {
            @"\.\s?",
            @"\.\.\.\s?",
            @"\?\s?",
            @"\!\s?",
            @"\?\!\s?"
        };

        public static readonly string[] SentenceSeparators = new[]
        {
            ". ",
            "... ",
            "? ",
            "! ",
            "?! "
        };

        public static readonly string[] LiteralPatterns = new[]
        {
            @"\w+",
            @"\w+\-\w+",
            @"\w+\'\w+"
        };

        public static readonly string[] Literals = new[]
        {
            @"\w+",
            @"\w+\-\w+",
            @"\w+\'\w+"
        };
    }
}
