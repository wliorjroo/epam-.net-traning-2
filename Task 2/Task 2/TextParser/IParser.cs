﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.Text;

namespace TextParser
{
    public interface IParser
    {
        IText Parse(string filePath);
    }
}
