﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextParser.Configuration;

namespace TextParser
{
    public static class Parser
    {
        public static IParser TextParser { get; private set; }

        static Parser()
        {
            // TODO Load settings
            //ConfigLoader.Load();
            TextParser = new TextParser.Parser();
        }
    }
}
