﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextComposition.Text;

namespace TextParser.Exceptions
{
    public class ParseException : Exception
    {
        public IText ParsedText;

        public ParseException()
        {
        }

        public ParseException(string message) : base(message)
        {
        }

        public ParseException(string message, IText text) : base(message)
        {
            ParsedText = text;
        }
    }
}
